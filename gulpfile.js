var gulp = require('gulp'); 
var rename = require('gulp-rename');

const { src, dest } = require("gulp");
gulp.task('copyfonts', function(done) {
  gulp.src('./node_modules/@fontsource/**/*{latin-ext,base}-{400,700}.css')
    .pipe(gulp.dest('./assets/css/font'));
  gulp.src('./node_modules/@openfonts/**/{index.css}')
    .pipe(gulp.dest('./assets/css/font'));
  gulp.src('./node_modules/@fontsource/*/files/*{latin-ext,base}-{400,700}*.{css,woff2,woff}') .pipe(rename({dirname: ''}))
    .pipe(gulp.dest('./static/font'));
  gulp.src('./node_modules/@openfonts/**/{*latin-ext-{400,700}*.{woff2,woff},index.css}')
    .pipe(gulp.dest('./static/font'));
  done();
});
