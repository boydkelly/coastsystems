#!/bin/bash
fonts=./assets/css/fonts.css
onlinefonts=./assets/css/fonts-online.css

cp $onlinefonts $fonts
# fix the path for the font files in all the font.css files
#find assets/css/font -type f -name "base*.css" -exec sed -i 's/\.\/files/\/font/g' {} \;
# then cat the files into a single fonts.css file in the assets folder
# (can't @import the files separately as they will not be picked up as asset resources.)
#find assets/css/font -type f -name "base*.css" -exec cat {} >> $fonts \;

