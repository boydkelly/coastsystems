#!/usr/bin/bash

main=./assets/css/main.css
fonts=./assets/css/fonts.css

sed -i '/^@import url(/d' $main 
sed -i '/^@import url(/d' $fonts 

[[ -f $fonts ]]  && rm $fonts
# fix the path for the font files in all the font.css files
find assets/css/font -type f -name "*.css" -exec sed -i 's/\.\/files/\/font/g' {} \;
# then cat the files into a single fonts.css file in the assets folder
# (can't @import the files separately as they will not be picked up as asset resources.)
find assets/css/font -type f -name "*.css" -exec cat {} >> $fonts \;

