fonts = ./assets/css/fonts.css
onlinefonts = ./assets/css/fonts-online.css

install :
	  npm install
		npm install hugo asciidoctor 

copyfonts :
		gulp copyfonts

online : copyfonts
		cp $(onlinefonts) $(fonts)
		find assets/css/font -type f -name "base*.css" -exec sed -i 's/\.\/files/\/font/g' {} \;
		find assets/css/font -type f -name "base*.css" -exec cat {} >> $(fonts) \;

offline : copyfonts
		[[ -f $(fonts) ]]  && rm $(fonts)
		find assets/css/font -type f -name "*.css" -exec sed -i 's/\.\/files/\/font/g' {} \;
		find assets/css/font -type f -name "*.css" -exec cat {} >> $(fonts) \;

start : modules server

download:
	./env.sh \hugo mod get -u ./...

publish: download
	git commit -a -m wip && git push

server:
	#./env.sh hugo server --destination=./public &
	./env.sh hugo server --disableFastRender --destination=./public &
