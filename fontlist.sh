#!/usr/bin/bash
#load fonts in scss
#fonts=./assets/scss/fonts.scss
#[[ -f $fonts ]]  && rm $fonts
#printf "%s%s\n" @charset ' "utf-8";' > $fonts

#find ./static/font/ -name *.css -exec sed -i '/font-display/g' {} \;

#The following generates a <partial> fonts.html file containing a table listing all the installed fonts
#out=./layouts/partials/fonts.html
text="Mɔgɔkɔrɔw kaɲi ka kɛ ni ŋania ɲuman ye"

for dest in partials shortcodes; do
  out="./layouts/${dest}/fonts.html"

  echo '<table class="table fonts is-size-3">' > $out
  echo '<tbody>' >> $out

  echo '<tr>' >> $out
  echo '<th>' >> $out
  printf "%s\n" "Font name" >> $out
  #  echo '{{ T "fontname" }}' >> $out
  echo '</th>' >> $out
  echo '<th>' >> $out
  printf "%s\n" "Sample" >> $out
  #  echo '{{ T "Sample" }}' >> $out
  echo '</th>' >> $out
  echo '</tr>' >> $out

# Make a table row with a style from each installed font
  grep -R font-family ${PWD}/assets/css/font/ | awk -F": " '{print $3}' | sort | uniq | sed 's/;//g' | sed "s/'//g" |
    while IFS= read -r line;
    do
      echo '<tr style="font-family:' "$line" ';">'  >> $out
      echo '<td>' >> $out
      echo $line >> $out
      echo '</td>' >> $out
      echo '<td style="font-family:' "$line" ';">'  >> $out
      echo "$text" >> $out 
      echo '</td>' >> $out
      echo '</tr>' >> $out
    done

    echo '</tbody>' >> $out
    echo '</table>' >> $out
    tidy -quiet -omit -m $out 2>/dev/null
    sed -i '1,4d' $out
  done
